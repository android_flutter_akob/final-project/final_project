import 'dart:js';

import 'package:final_project_2/electric_page.dart';
import 'package:final_project_2/setting.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  runApp(MaterialApp(
    title: 'Electric Calculator',
    initialRoute: '/',
    routes: {
      '/': (context) => const MyApp(),
      '/setting': (context) => SettingWidget(),
      '/fridge': (context) => fridge(),
      '/fan': (context) => fan(),
      '/tv': (context) => tv(),
      '/air': (context) => air(),
      '/computer': (context) => computer(),
      '/washing': (context) => washing(),
      '/combLight': (context) => combLight(),
      '/light': (context) => light(),
      '/digtitalBox': (context) => digtitalBox(),
    },
  ));
}

Widget _menuElectric(name, press) {
  return ListTile(
    title: Text('$name'),
    onTap: press,
  );
}

Widget _showCalculate(name, sumed, endUnit) {
  return ListTile(
    title: Text(
      '$name ใช้ไป ${sumed.toStringAsFixed(2)} $endUnit',
      style: TextStyle(
          color: (sumed > 2)
              ? Colors.red.withOpacity(0.6)
              : Colors.black.withOpacity(0.6),
          fontSize: 13.0),
    ),
  );
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  int check = 0;
  // 0 -> Day | 1 -> Month
  String endBathUnit = "ต่อวัน";
  String endUnit = "หน่วยต่อวัน";
  String name = "";
  String equim = "";
  double wathPerUnit = 0.0;
  double sumMoney = 0.0;
  double fridge = 0.0;
  double digtitalBox = 0.0;
  double light = 0.0;
  double combLight = 0.0;
  double washing = 0.0;
  double computer = 0.0;
  double air = 0.0;
  double tv = 0.0;
  double fan = 0.0;
  double sumMoneyMonth = 0.0;
  double fridgeMonth = 0.0;
  double digtitalBoxMonth = 0.0;
  double lightMonth = 0.0;
  double combLightMonth = 0.0;
  double washingMonth = 0.0;
  double computerMonth = 0.0;
  double airMonth = 0.0;
  double tvMonth = 0.0;
  double fanMonth = 0.0;

  @override
  void initState() {
    super.initState();
    _loadSetting();
    getInMonth();
  }

  Future<void> _loadSetting() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      name = prefs.getString('name') ?? 'Guest';
      equim = prefs.getString('equim') ?? 'ยังไม่ได้ตั้งค่า';
      wathPerUnit = prefs.getDouble('wathPerUnit') ?? 4.42;
    });
  }

  Future<void> _loadElectric() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      check = 0;
      endBathUnit = "ต่อวัน";
      endUnit = "หน่วยต่อวัน";
      fridge = prefs.getDouble('fridge') ?? 0.0;
      digtitalBox = prefs.getDouble('digtitalBox') ?? 0.0;
      light = prefs.getDouble('light') ?? 0.0;
      combLight = prefs.getDouble('combLight') ?? 0.0;
      washing = prefs.getDouble('washing') ?? 0.0;
      computer = prefs.getDouble('computer') ?? 0.0;
      air = prefs.getDouble('air') ?? 0.0;
      tv = prefs.getDouble('tv') ?? 0.0;
      fan = prefs.getDouble('fan') ?? 0.0;

      fan = prefs.getDouble('fan') ?? 0.0;
      sumMoney = (fridge +
              digtitalBox +
              light +
              combLight +
              washing +
              computer +
              air +
              tv) *
          wathPerUnit;
    });
  }

  void getInMonth() {
    setState(() {
      fridgeMonth = fridge * 30;
      digtitalBoxMonth = digtitalBox * 30;
      lightMonth = light * 30;
      combLightMonth = combLight * 30;
      washingMonth = washing * 30;
      computerMonth = computer * 30;
      airMonth = air * 30;
      tvMonth = tv * 30;
      fanMonth = fan * 30;
      sumMoneyMonth = sumMoney * 30;
    });
  }

  Future<void> _resetSetting() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove('name');
    prefs.remove('equim');
    prefs.remove('fridge');
    prefs.remove('fan');
    prefs.remove('digtitalBox');
    prefs.remove('light');
    prefs.remove('combLight');
    prefs.remove('washing');
    prefs.remove('computer');
    prefs.remove('air');
    prefs.remove('tv');
    prefs.remove('wathPerUnit');
    await _loadSetting();
  }

  @override
  Widget build(BuildContext context) {
    Color color = Theme.of(context).primaryColor;

    void changeUnitEle() {
      // 0-> Day | 1-> Month
      if (check == 0) {
        setState(() {
          check = 1;
          endBathUnit = "ต่อเดือน";
          endUnit = "หน่วยต่อเดือน";
          fridge = fridge * 30;
          digtitalBox = digtitalBox * 30;
          light = light * 30;
          combLight = combLight * 30;
          washing = washing * 30;
          computer = computer * 30;
          air = air * 30;
          tv = tv * 30;
          fan = fan * 30;
          sumMoney = sumMoney * 30;
        });
      } else {
        setState(() {
          check = 0;
          endBathUnit = "ต่อวัน";
          endUnit = "หน่วยต่อวัน";
          fridge = fridge / 30;
          digtitalBox = digtitalBox / 30;
          light = light / 30;
          combLight = combLight / 30;
          washing = washing / 30;
          computer = computer / 30;
          air = air / 30;
          tv = tv / 30;
          fan = fan / 30;
          sumMoney = sumMoney / 30;
        });
      }
    }

    return Scaffold(
      appBar: AppBar(
        title: Text('Electric Calculator'),
      ),
      body: ListView(
        children: [
          Card(
            clipBehavior: Clip.antiAlias,
            child: Column(
              children: [
                ListTile(
                  leading: Icon(Icons.person),
                  title: Text("$name"),
                  subtitle: Text(
                    'ประเภท: $equim  -- ค่าไฟ : $wathPerUnit บาทต่อหน่วย',
                    style: TextStyle(color: Colors.black.withOpacity(0.6)),
                  ),
                ),
                _showCalculate("ตู้เย็น", fridge, endUnit),
                _showCalculate("พัดลม", fan, endUnit),
                _showCalculate("ทีวี", tv, endUnit),
                _showCalculate("แอร์ - เครื่องปรับอากาศ", air, endUnit),
                _showCalculate("คอมพิวเตอร์", computer, endUnit),
                _showCalculate("เครื่องซักผ้า", washing, endUnit),
                _showCalculate("โคมไฟ", combLight, endUnit),
                _showCalculate("ไฟบ้าน", light, endUnit),
                _showCalculate("กล่องดิจิตอล", digtitalBox, endUnit),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Center(
                    child: Text(
                      '฿ ${sumMoney.toStringAsFixed(2)} $endBathUnit & ${sumMoneyMonth.toStringAsFixed(2)} ต่อเดือน',
                      style: TextStyle(
                          color: Colors.black.withOpacity(0.6), fontSize: 20.0),
                    ),
                  ),
                ),
                /* ButtonBar(
                  alignment: MainAxisAlignment.end,
                  children: [
                    TextButton(
                      onPressed: () {},
                      child: const Text('Reset'),
                    ),
                  ],
                ), */
              ],
            ),
          ),
          Column(
            children: [
              Container(
                padding: const EdgeInsets.all(20.0),
                child: Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          IconButton(
                            icon: Icon(
                              Icons.settings,
                              color: color,
                            ),
                            onPressed: () async {
                              await Navigator.pushNamed(context, '/setting');
                              await _loadSetting();
                              await _loadElectric();
                            },
                          ),
                          Container(
                            child: TextButton(
                              onPressed: () async {
                                await Navigator.pushNamed(context, '/setting');
                                await _loadSetting();
                                await _loadElectric();
                              },
                              child: Text("การตั้งค่า",
                                  style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400,
                                    color: color,
                                  )),
                            ),
                          ),
                        ],
                      ),
                      /* Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          IconButton(
                            icon: Icon(
                              Icons.cached,
                              color: color,
                            ),
                            onPressed: () {
                              changeUnitEle();
                            },
                          ),
                          Container(
                            child: TextButton(
                              onPressed: () {
                                changeUnitEle();
                              },
                              child: Text("เปลื่ยน$endUnit ",
                                  style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400,
                                    color: color,
                                  )),
                            ),
                          ),
                        ],
                      ), */
                      Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          IconButton(
                            icon: Icon(
                              Icons.restore_from_trash_sharp,
                              color: color,
                            ),
                            onPressed: () {
                              _resetSetting();
                              _loadElectric();
                            },
                          ),
                          Container(
                            child: TextButton(
                              onPressed: () {
                                _resetSetting();
                                _loadElectric();
                              },
                              child: Text("คืนค่าเดิม",
                                  style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400,
                                    color: color,
                                  )),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
          _menuElectric("ตู้เย็น", () async {
            //
            await Navigator.pushNamed(context, '/fridge');
            await _loadElectric();
            getInMonth();
          }),
          _menuElectric("พัดลม", () async {
            await Navigator.pushNamed(context, '/fan');
            await _loadElectric();
            getInMonth();
          }), //fan
          _menuElectric("ทีวี", () async {
            await Navigator.pushNamed(context, '/tv');
            await _loadElectric();
            getInMonth();
          }), //tv
          _menuElectric("แอร์ - เครื่องปรับอากาศ", () async {
            await Navigator.pushNamed(context, '/air');
            await _loadElectric();
            getInMonth();
          }), //air
          _menuElectric("คอมพิวเตอร์", () async {
            await Navigator.pushNamed(context, '/computer');
            await _loadElectric();
            getInMonth();
          }), //computer
          _menuElectric("เครื่องซักผ้า", () async {
            await Navigator.pushNamed(context, '/washing');
            await _loadElectric();
            getInMonth();
          }), //washing
          _menuElectric("โคมไฟ", () async {
            await Navigator.pushNamed(context, '/combLight');
            await _loadElectric();
            getInMonth();
          }), //combLight
          _menuElectric("ไฟบ้าน", () async {
            await Navigator.pushNamed(context, '/light');
            await _loadElectric();
            getInMonth();
          }), //light
          _menuElectric("กล่องดิจิตอล", () async {
            await Navigator.pushNamed(context, '/digtitalBox');
            await _loadElectric();
            getInMonth();
          }), //digtitalBox
        ],
      ),
    );
  }
}
