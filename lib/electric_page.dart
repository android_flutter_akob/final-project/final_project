import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

/*-----------------------------------------------------------------*/

final _formKey = GlobalKey<FormState>();
int wat = 0;
int countEle = 0;
int hours = 0;
double unit = 0.0;

final _watController = TextEditingController();
final _counteleController = TextEditingController();
final _hoursController = TextEditingController();

/*-------------------------------------------------------*/

class fridge extends StatefulWidget {
  fridge({Key? key}) : super(key: key);

  @override
  _fridgeState createState() => _fridgeState();
}

class _fridgeState extends State<fridge> {

  Future<void> _saveWork(String name) async {
    
    setState(() {
      unit = ((wat * countEle) / 1000) * hours; 
    }); 
    final prefs = await SharedPreferences.getInstance(); 
    setState(() {
        prefs.setDouble("$name", unit);
    });
  }

  Widget formElectric(String name){
  return Form(
        key: _formKey,
        child: Container(
          padding: EdgeInsets.all(16.0),
          child: ListView(
            children: [
              Image.network("https://cf.shopee.co.th/file/55cd7e74b96c89752f17a7fcb60e7df2"),
              TextFormField(
                controller: _watController,
                validator: (value) {
                  var num = int.tryParse(value!);
                  if (num == null || num <= 0) {
                    return 'กรุณาใส่กำลังไฟฟ้า (วัตต์) ที่มีค่ามากกว่าหรือเท่ากับ 0';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(labelText: 'กำลังไฟฟ้า (วัตต์)'),
                keyboardType: TextInputType.number,
                onChanged: (value) {
                  setState(() {
                    wat = int.tryParse(value)!;
                  });
                },
                
              ),
              TextFormField(
                controller: _counteleController,
                validator: (value) {
                  var num = int.tryParse(value!);
                  if (num == null || num <= 0) {
                    return 'กรุณาจำนวนเครื่องใช้ไฟฟ้า ที่มีค่ามากกว่าหรือเท่ากับ 0';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(labelText: 'จำนวนเครื่องใช้ไฟฟ้า (ชิ้น)'),
                onChanged: (value) {
                  setState(() {
                    countEle = int.tryParse(value)!;
                  });
                },
                keyboardType: TextInputType.number,
              ),
              TextFormField(
                controller: _hoursController,
                validator: (value) {
                  var num = int.tryParse(value!);
                  if (num == null || num <= 0) {
                    return 'กรุณาจำนวนชั่วโมงที่ใช้ต่อวัน ที่มีค่ามากกว่าหรือเท่ากับ 0';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(labelText: 'จำนวนชั่วโมงที่ใช้ต่อวัน'),
                onChanged: (value) {
                  setState(() {
                    hours = int.tryParse(value)!;
                  });
                },
                keyboardType: TextInputType.number,
              ),
              ElevatedButton(
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      _saveWork(name);
                      Navigator.pop(context);
                    }
                  },
                  child: Text('Save')),
            ],
          ),
        ),
      );
}


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("ตู้เย็น"),
      ),
      body: formElectric("fridge"),
    );
  }
}

/*-------------------------------------------------------*/

class fan extends StatefulWidget {
  fan({Key? key}) : super(key: key);

  @override
  _fanState createState() => _fanState();
}



class _fanState extends State<fan> {
   Future<void> _saveWork(String name) async {
    setState(() {
      unit = ((wat * countEle) / 1000) * hours; 
    }); 
    final prefs = await SharedPreferences.getInstance(); 
    setState(() {
        prefs.setDouble("$name", unit);
    });
  }

  Widget formElectric(String name){
  return Form(
        key: _formKey,
        child: Container(
          padding: EdgeInsets.all(16.0),
          child: ListView(
            children: [
              Image.network("https://img10.jd.co.th/n0/jfs/t34/80/3309086649/246844/3fe2f52a/5f3e3bddN87c79b0d.jpg!q70.jpg"),//
              TextFormField(
                controller: _watController,
                validator: (value) {
                  var num = int.tryParse(value!);
                  if (num == null || num <= 0) {
                    return 'กรุณาใส่กำลังไฟฟ้า (วัตต์) ที่มีค่ามากกว่าหรือเท่ากับ 0';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(labelText: 'กำลังไฟฟ้า (วัตต์)'),
                keyboardType: TextInputType.number,
                onChanged: (value) {
                  setState(() {
                    wat = int.tryParse(value)!;
                  });
                },
                
              ),
              TextFormField(
                controller: _counteleController,
                validator: (value) {
                  var num = int.tryParse(value!);
                  if (num == null || num <= 0) {
                    return 'กรุณาจำนวนเครื่องใช้ไฟฟ้า ที่มีค่ามากกว่าหรือเท่ากับ 0';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(labelText: 'จำนวนเครื่องใช้ไฟฟ้า (ชิ้น)'),
                onChanged: (value) {
                  setState(() {
                    countEle = int.tryParse(value)!;
                  });
                },
                keyboardType: TextInputType.number,
              ),
              TextFormField(
                controller: _hoursController,
                validator: (value) {
                  var num = int.tryParse(value!);
                  if (num == null || num <= 0) {
                    return 'กรุณาจำนวนชั่วโมงที่ใช้ต่อวัน ที่มีค่ามากกว่าหรือเท่ากับ 0';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(labelText: 'จำนวนชั่วโมงที่ใช้ต่อวัน'),
                onChanged: (value) {
                  setState(() {
                    hours = int.tryParse(value)!;
                  });
                },
                keyboardType: TextInputType.number,
              ),
              ElevatedButton(
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      _saveWork(name);
                      Navigator.pop(context);
                    }
                  },
                  child: Text('Save')),
            ],
          ),
        ),
      );
}


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("พัดลม"),
      ),
      body: formElectric("fan"),
    );
  }
}

/*-------------------------------------------------------*/

class tv extends StatefulWidget {
  tv({Key? key}) : super(key: key);

  @override
  _tvState createState() => _tvState();
}

class _tvState extends State<tv> {
   Future<void> _saveWork(String name) async {
    setState(() {
      unit = ((wat * countEle) / 1000) * hours; 
    }); 
    final prefs = await SharedPreferences.getInstance(); 
    setState(() {
        prefs.setDouble("$name", unit);
    });
  }

  Widget formElectric(String name){
  return Form(
        key: _formKey,
        child: Container(
          padding: EdgeInsets.all(16.0),
          child: ListView(
            children: [
              Image.network("https://mpics.mgronline.com/pics/Images/564000005262001.JPEG"),
              TextFormField(
                controller: _watController,
                validator: (value) {
                  var num = int.tryParse(value!);
                  if (num == null || num <= 0) {
                    return 'กรุณาใส่กำลังไฟฟ้า (วัตต์) ที่มีค่ามากกว่าหรือเท่ากับ 0';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(labelText: 'กำลังไฟฟ้า (วัตต์)'),
                keyboardType: TextInputType.number,
                onChanged: (value) {
                  setState(() {
                    wat = int.tryParse(value)!;
                  });
                },
                
              ),
              TextFormField(
                controller: _counteleController,
                validator: (value) {
                  var num = int.tryParse(value!);
                  if (num == null || num <= 0) {
                    return 'กรุณาจำนวนเครื่องใช้ไฟฟ้า ที่มีค่ามากกว่าหรือเท่ากับ 0';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(labelText: 'จำนวนเครื่องใช้ไฟฟ้า (ชิ้น)'),
                onChanged: (value) {
                  setState(() {
                    countEle = int.tryParse(value)!;
                  });
                },
                keyboardType: TextInputType.number,
              ),
              TextFormField(
                controller: _hoursController,
                validator: (value) {
                  var num = int.tryParse(value!);
                  if (num == null || num <= 0) {
                    return 'กรุณาจำนวนชั่วโมงที่ใช้ต่อวัน ที่มีค่ามากกว่าหรือเท่ากับ 0';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(labelText: 'จำนวนชั่วโมงที่ใช้ต่อวัน'),
                onChanged: (value) {
                  setState(() {
                    hours = int.tryParse(value)!;
                  });
                },
                keyboardType: TextInputType.number,
              ),
              ElevatedButton(
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      _saveWork(name);
                      Navigator.pop(context);
                    }
                  },
                  child: Text('Save')),
            ],
          ),
        ),
      );
}


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("ทีวี"),
      ),
      body: formElectric("tv"),
    );
  }
}

/*-------------------------------------------------------*/

class air extends StatefulWidget {
  air({Key? key}) : super(key: key);

  @override
  _airState createState() => _airState();
}

class _airState extends State<air> {
   Future<void> _saveWork(String name) async {
    setState(() {
      unit = ((wat * countEle) / 1000) * hours; 
    }); 
    final prefs = await SharedPreferences.getInstance(); 
    setState(() {
        prefs.setDouble("$name", unit);
    });
  }

  Widget formElectric(String name){
  return Form(
        key: _formKey,
        child: Container(
          padding: EdgeInsets.all(16.0),
          child: ListView(
            children: [
              Image.network("https://www.lg.com/th/images/air-conditioner-inverter/md06086719/gallery/medium02.jpg"),//
              TextFormField(
                controller: _watController,
                validator: (value) {
                  var num = int.tryParse(value!);
                  if (num == null || num <= 0) {
                    return 'กรุณาใส่กำลังไฟฟ้า (วัตต์) ที่มีค่ามากกว่าหรือเท่ากับ 0';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(labelText: 'กำลังไฟฟ้า (วัตต์)'),
                keyboardType: TextInputType.number,
                onChanged: (value) {
                  setState(() {
                    wat = int.tryParse(value)!;
                  });
                },
                
              ),
              TextFormField(
                controller: _counteleController,
                validator: (value) {
                  var num = int.tryParse(value!);
                  if (num == null || num <= 0) {
                    return 'กรุณาจำนวนเครื่องใช้ไฟฟ้า ที่มีค่ามากกว่าหรือเท่ากับ 0';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(labelText: 'จำนวนเครื่องใช้ไฟฟ้า (ชิ้น)'),
                onChanged: (value) {
                  setState(() {
                    countEle = int.tryParse(value)!;
                  });
                },
                keyboardType: TextInputType.number,
              ),
              TextFormField(
                controller: _hoursController,
                validator: (value) {
                  var num = int.tryParse(value!);
                  if (num == null || num <= 0) {
                    return 'กรุณาจำนวนชั่วโมงที่ใช้ต่อวัน ที่มีค่ามากกว่าหรือเท่ากับ 0';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(labelText: 'จำนวนชั่วโมงที่ใช้ต่อวัน'),
                onChanged: (value) {
                  setState(() {
                    hours = int.tryParse(value)!;
                  });
                },
                keyboardType: TextInputType.number,
              ),
              ElevatedButton(
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      _saveWork(name);
                      Navigator.pop(context);
                    }
                  },
                  child: Text('Save')),
            ],
          ),
        ),
      );
}


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("แอร์ - เครื่องปรับอากาศ"),
      ),
      body: formElectric("air"),
    );
  }
}

/*-------------------------------------------------------*/

class computer extends StatefulWidget {
  computer({Key? key}) : super(key: key);

  @override
  _computerState createState() => _computerState();
}

class _computerState extends State<computer> {
   Future<void> _saveWork(String name) async {
    setState(() {
      unit = ((wat * countEle) / 1000) * hours; 
    }); 
    final prefs = await SharedPreferences.getInstance(); 
    setState(() {
        prefs.setDouble("$name", unit);
    });
  }

  Widget formElectric(String name){
  return Form(
        key: _formKey,
        child: Container(
          padding: EdgeInsets.all(16.0),
          child: ListView(
            children: [
              Image.network("https://th-test-11.slatic.net/p/bfa6e863ef11ae68b7161b251697fe89.jpg"),//
              TextFormField(
                controller: _watController,
                validator: (value) {
                  var num = int.tryParse(value!);
                  if (num == null || num <= 0) {
                    return 'กรุณาใส่กำลังไฟฟ้า (วัตต์) ที่มีค่ามากกว่าหรือเท่ากับ 0';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(labelText: 'กำลังไฟฟ้า (วัตต์)'),
                keyboardType: TextInputType.number,
                onChanged: (value) {
                  setState(() {
                    wat = int.tryParse(value)!;
                  });
                },
                
              ),
              TextFormField(
                controller: _counteleController,
                validator: (value) {
                  var num = int.tryParse(value!);
                  if (num == null || num <= 0) {
                    return 'กรุณาจำนวนเครื่องใช้ไฟฟ้า ที่มีค่ามากกว่าหรือเท่ากับ 0';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(labelText: 'จำนวนเครื่องใช้ไฟฟ้า (ชิ้น)'),
                onChanged: (value) {
                  setState(() {
                    countEle = int.tryParse(value)!;
                  });
                },
                keyboardType: TextInputType.number,
              ),
              TextFormField(
                controller: _hoursController,
                validator: (value) {
                  var num = int.tryParse(value!);
                  if (num == null || num <= 0) {
                    return 'กรุณาจำนวนชั่วโมงที่ใช้ต่อวัน ที่มีค่ามากกว่าหรือเท่ากับ 0';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(labelText: 'จำนวนชั่วโมงที่ใช้ต่อวัน'),
                onChanged: (value) {
                  setState(() {
                    hours = int.tryParse(value)!;
                  });
                },
                keyboardType: TextInputType.number,
              ),
              ElevatedButton(
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      _saveWork(name);
                      Navigator.pop(context);
                    }
                  },
                  child: Text('Save')),
            ],
          ),
        ),
      );
}


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("คอมพิวเตอร์"),
      ),
      body: formElectric("computer"),
    );
  }
}

/*-------------------------------------------------------*/

class washing extends StatefulWidget {
  washing({Key? key}) : super(key: key);

  @override
  _washingState createState() => _washingState();
}

class _washingState extends State<washing> {
   Future<void> _saveWork(String name) async {
    setState(() {
      unit = ((wat * countEle) / 1000) * hours; 
    }); 
    final prefs = await SharedPreferences.getInstance(); 
    setState(() {
        prefs.setDouble("$name", unit);
    });
  }

  Widget formElectric(String name){
  return Form(
        key: _formKey,
        child: Container(
          padding: EdgeInsets.all(16.0),
          child: ListView(
            children: [
              Image.network("https://images.samsung.com/is/image/samsung/th-top-loading-wa16r6380bvst-wa16r6380bv-st-frontblack-224163013?\$720_576_PNG\$"),//
              TextFormField(
                controller: _watController,
                validator: (value) {
                  var num = int.tryParse(value!);
                  if (num == null || num <= 0) {
                    return 'กรุณาใส่กำลังไฟฟ้า (วัตต์) ที่มีค่ามากกว่าหรือเท่ากับ 0';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(labelText: 'กำลังไฟฟ้า (วัตต์)'),
                keyboardType: TextInputType.number,
                onChanged: (value) {
                  setState(() {
                    wat = int.tryParse(value)!;
                  });
                },
                
              ),
              TextFormField(
                controller: _counteleController,
                validator: (value) {
                  var num = int.tryParse(value!);
                  if (num == null || num <= 0) {
                    return 'กรุณาจำนวนเครื่องใช้ไฟฟ้า ที่มีค่ามากกว่าหรือเท่ากับ 0';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(labelText: 'จำนวนเครื่องใช้ไฟฟ้า (ชิ้น)'),
                onChanged: (value) {
                  setState(() {
                    countEle = int.tryParse(value)!;
                  });
                },
                keyboardType: TextInputType.number,
              ),
              TextFormField(
                controller: _hoursController,
                validator: (value) {
                  var num = int.tryParse(value!);
                  if (num == null || num <= 0) {
                    return 'กรุณาจำนวนชั่วโมงที่ใช้ต่อวัน ที่มีค่ามากกว่าหรือเท่ากับ 0';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(labelText: 'จำนวนชั่วโมงที่ใช้ต่อวัน'),
                onChanged: (value) {
                  setState(() {
                    hours = int.tryParse(value)!;
                  });
                },
                keyboardType: TextInputType.number,
              ),
              ElevatedButton(
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      _saveWork(name);
                      Navigator.pop(context);
                    }
                  },
                  child: Text('Save')),
            ],
          ),
        ),
      );
}


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("เครื่องซักผ้า"),
      ),
      body: formElectric("washing"),
    );
  }
}

/*-------------------------------------------------------*/

class combLight extends StatefulWidget {
  combLight({Key? key}) : super(key: key);

  @override
  _combLightState createState() => _combLightState();
}

class _combLightState extends State<combLight> {
   Future<void> _saveWork(String name) async {
    setState(() {
      unit = ((wat * countEle) / 1000) * hours; 
    }); 
    final prefs = await SharedPreferences.getInstance(); 
    setState(() {
        prefs.setDouble("$name", unit);
    });
  }

  Widget formElectric(String name){
  return Form(
        key: _formKey,
        child: Container(
          padding: EdgeInsets.all(16.0),
          child: ListView(
            children: [
              Image.network("https://th-live-05.slatic.net/p/17df8e7ac4e1c031567fb567659c66bf.png_720x720q80.jpg_.webp"),
              TextFormField(
                controller: _watController,
                validator: (value) {
                  var num = int.tryParse(value!);
                  if (num == null || num <= 0) {
                    return 'กรุณาใส่กำลังไฟฟ้า (วัตต์) ที่มีค่ามากกว่าหรือเท่ากับ 0';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(labelText: 'กำลังไฟฟ้า (วัตต์)'),
                keyboardType: TextInputType.number,
                onChanged: (value) {
                  setState(() {
                    wat = int.tryParse(value)!;
                  });
                },
                
              ),
              TextFormField(
                controller: _counteleController,
                validator: (value) {
                  var num = int.tryParse(value!);
                  if (num == null || num <= 0) {
                    return 'กรุณาจำนวนเครื่องใช้ไฟฟ้า ที่มีค่ามากกว่าหรือเท่ากับ 0';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(labelText: 'จำนวนเครื่องใช้ไฟฟ้า (ชิ้น)'),
                onChanged: (value) {
                  setState(() {
                    countEle = int.tryParse(value)!;
                  });
                },
                keyboardType: TextInputType.number,
              ),
              TextFormField(
                controller: _hoursController,
                validator: (value) {
                  var num = int.tryParse(value!);
                  if (num == null || num <= 0) {
                    return 'กรุณาจำนวนชั่วโมงที่ใช้ต่อวัน ที่มีค่ามากกว่าหรือเท่ากับ 0';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(labelText: 'จำนวนชั่วโมงที่ใช้ต่อวัน'),
                onChanged: (value) {
                  setState(() {
                    hours = int.tryParse(value)!;
                  });
                },
                keyboardType: TextInputType.number,
              ),
              ElevatedButton(
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      _saveWork(name);
                      Navigator.pop(context);
                    }
                  },
                  child: Text('Save')),
            ],
          ),
        ),
      );
}


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("โคมไฟ"),
      ),
      body: formElectric("combLight"),
    );
  }
}

/*-------------------------------------------------------*/

class light extends StatefulWidget {
  light({Key? key}) : super(key: key);

  @override
  _lightState createState() => _lightState();
}

class _lightState extends State<light> {
   Future<void> _saveWork(String name) async {
    setState(() {
      unit = ((wat * countEle) / 1000) * hours; 
    }); 
    final prefs = await SharedPreferences.getInstance(); 
    setState(() {
        prefs.setDouble("$name", unit);
    });
  }

  Widget formElectric(String name){
  return Form(
        key: _formKey,
        child: Container(
          padding: EdgeInsets.all(16.0),
          child: ListView(
            children: [
              Image.network("https://image.makewebeasy.net/makeweb/0/u5QBfcnUw/content/%E0%B8%88%E0%B8%B1%E0%B8%94%E0%B9%84%E0%B8%9F%E0%B9%83%E0%B8%99%E0%B8%9A%E0%B9%89%E0%B8%B2%E0%B8%999_650.jpg"),//
              TextFormField(
                controller: _watController,
                validator: (value) {
                  var num = int.tryParse(value!);
                  if (num == null || num <= 0) {
                    return 'กรุณาใส่กำลังไฟฟ้า (วัตต์) ที่มีค่ามากกว่าหรือเท่ากับ 0';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(labelText: 'กำลังไฟฟ้า (วัตต์)'),
                keyboardType: TextInputType.number,
                onChanged: (value) {
                  setState(() {
                    wat = int.tryParse(value)!;
                  });
                },
                
              ),
              TextFormField(
                controller: _counteleController,
                validator: (value) {
                  var num = int.tryParse(value!);
                  if (num == null || num <= 0) {
                    return 'กรุณาจำนวนเครื่องใช้ไฟฟ้า ที่มีค่ามากกว่าหรือเท่ากับ 0';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(labelText: 'จำนวนเครื่องใช้ไฟฟ้า (ชิ้น)'),
                onChanged: (value) {
                  setState(() {
                    countEle = int.tryParse(value)!;
                  });
                },
                keyboardType: TextInputType.number,
              ),
              TextFormField(
                controller: _hoursController,
                validator: (value) {
                  var num = int.tryParse(value!);
                  if (num == null || num <= 0) {
                    return 'กรุณาจำนวนชั่วโมงที่ใช้ต่อวัน ที่มีค่ามากกว่าหรือเท่ากับ 0';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(labelText: 'จำนวนชั่วโมงที่ใช้ต่อวัน'),
                onChanged: (value) {
                  setState(() {
                    hours = int.tryParse(value)!;
                  });
                },
                keyboardType: TextInputType.number,
              ),
              ElevatedButton(
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      _saveWork(name);
                      Navigator.pop(context);
                    }
                  },
                  child: Text('Save')),
            ],
          ),
        ),
      );
}


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("ไฟบ้าน"),
      ),
      body: formElectric("light"),
    );
  }
}

/*-------------------------------------------------------*/

class digtitalBox extends StatefulWidget {
  digtitalBox({Key? key}) : super(key: key);

  @override
  _digtitalBoxState createState() => _digtitalBoxState();
}

class _digtitalBoxState extends State<digtitalBox> {
   Future<void> _saveWork(String name) async {
    setState(() {
      unit = ((wat * countEle) / 1000) * hours; 
    }); 
    final prefs = await SharedPreferences.getInstance(); 
    setState(() {
        prefs.setDouble("$name", unit);
    });
  }

  Widget formElectric(String name){
  return Form(
        key: _formKey,
        child: Container(
          padding: EdgeInsets.all(16.0),
          child: ListView(
            children: [
              Image.network("https://image.makewebeasy.net/makeweb/0/IwrQAhi7s/7-11/%E0%B8%81%E0%B8%A5%E0%B9%88%E0%B8%AD%E0%B8%87%E0%B8%94%E0%B8%B4%E0%B8%88%E0%B8%B4%E0%B8%95%E0%B8%AD%E0%B8%A5%E0%B8%97%E0%B8%B5%E0%B8%A7%E0%B8%B5_SONORE_%E0%B9%80%E0%B8%AA%E0%B8%B2%E0%B8%AD%E0%B8%B2%E0%B8%81%E0%B8%B2%E0%B8%A8_DD6_%E0%B8%9E%E0%B8%A3%E0%B9%89%E0%B8%AD%E0%B8%A1%E0%B8%AA%E0%B8%B2%E0%B8%A2_1_600.png"),
              TextFormField(
                controller: _watController,
                validator: (value) {
                  var num = int.tryParse(value!);
                  if (num == null || num <= 0) {
                    return 'กรุณาใส่กำลังไฟฟ้า (วัตต์) ที่มีค่ามากกว่าหรือเท่ากับ 0';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(labelText: 'กำลังไฟฟ้า (วัตต์)'),
                keyboardType: TextInputType.number,
                onChanged: (value) {
                  setState(() {
                    wat = int.tryParse(value)!;
                  });
                },
                
              ),
              TextFormField(
                controller: _counteleController,
                validator: (value) {
                  var num = int.tryParse(value!);
                  if (num == null || num <= 0) {
                    return 'กรุณาจำนวนเครื่องใช้ไฟฟ้า ที่มีค่ามากกว่าหรือเท่ากับ 0';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(labelText: 'จำนวนเครื่องใช้ไฟฟ้า (ชิ้น)'),
                onChanged: (value) {
                  setState(() {
                    countEle = int.tryParse(value)!;
                  });
                },
                keyboardType: TextInputType.number,
              ),
              TextFormField(
                controller: _hoursController,
                validator: (value) {
                  var num = int.tryParse(value!);
                  if (num == null || num <= 0) {
                    return 'กรุณาจำนวนชั่วโมงที่ใช้ต่อวัน ที่มีค่ามากกว่าหรือเท่ากับ 0';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(labelText: 'จำนวนชั่วโมงที่ใช้ต่อวัน'),
                onChanged: (value) {
                  setState(() {
                    hours = int.tryParse(value)!;
                  });
                },
                keyboardType: TextInputType.number,
              ),
              ElevatedButton(
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      _saveWork(name);
                      Navigator.pop(context);
                    }
                  },
                  child: Text('Save')),
            ],
          ),
        ),
      );
}


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("กล่องดิจิตอล"),
      ),
      body: formElectric("digtitalBox"),
    );
  }
}

