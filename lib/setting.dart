import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SettingWidget extends StatefulWidget {
  SettingWidget({Key? key}) : super(key: key);

  @override
  _SettingWidgetState createState() => _SettingWidgetState();
}

final _formKey = GlobalKey<FormState>();
String name = "";
String equim = "";
double wathPerUnit = 0.0;

final _nameController = TextEditingController();
final _equimController = TextEditingController();
final _wathPerUnitController = TextEditingController();

class _SettingWidgetState extends State<SettingWidget> {
  @override
  void initState() {
    super.initState();
    _loadSetting();
  }

  Future<void> _loadSetting() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      name = prefs.getString('name') ?? 'Guest';
      _nameController.text = name;
      equim = prefs.getString('equim') ?? 'บ้าน';
      _equimController.text = equim;
      wathPerUnit = prefs.getDouble('wathPerUnit') ?? 4.42;
      _wathPerUnitController.text = '$wathPerUnit';
    });
  }

  Future<void> _saveSetting() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      prefs.setString('name', name);
      prefs.setString('equim', equim);
      prefs.setDouble('wathPerUnit', wathPerUnit);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Setting"),
      ),
      body: Form(
        key: _formKey,
        child: Container(
          padding: EdgeInsets.all(16.0),
          child: ListView(
            children: [
              TextFormField(
                autofocus: true,
                controller: _nameController,
                validator: (value) {
                  if (value == null || value.isEmpty || value.length < 5) {
                    return 'กรุณาใส่ข้อมูลชื่อ';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(labelText: 'กรุณาใส่ชื่อ'),
                onChanged: (value) {
                  setState(() {
                    name = value;
                  });
                },
              ),
              TextFormField(
                controller: _equimController,
                validator: (value) {
                  if (value == null || value.isEmpty || value.length < 2) {
                    return 'กรุณาใส่ประเภทของที่พัก เช่น บ้าน คอนโด หอพัก โรงเเรม';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration:
                    InputDecoration(labelText: 'กรุณาใส่ประเภทของที่พัก'),
                onChanged: (value) {
                  setState(() {
                    equim = value;
                  });
                },
              ),
              TextFormField(
                controller: _wathPerUnitController,
                validator: (value) {
                  var num = double.parse(value!);
                  if (num == null || num <= 0) {
                    return 'กรุณาใส่หน่วยไฟฟ้าที่มีค่ามากกว่าหรือเท่ากับ 0';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(labelText: 'ค่าไฟฟ้าต่อหน่วย'),
                onChanged: (value) {
                  setState(() {
                    double conv = double.parse(value);
                    if (conv.isNaN) {
                      wathPerUnit = 4.42;
                    } else {
                      wathPerUnit = conv;
                    }
                  });
                },
                keyboardType: TextInputType.number,
              ),
              ElevatedButton(
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      _saveSetting();
                      Navigator.pop(context);
                    }
                  },
                  child: Text('Save')),
            ],
          ),
        ),
      ),
    );
  }
}
